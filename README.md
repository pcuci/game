# Game

We play the structured feedback game to quickly cover many topics and gain diverse reactions from a highly attentive audience.

## Interaction Protocol

If you have questions, ask your contact before the meeting.
Join the online meeting
  (or be at the in-person meeting)

Your contact introduces you to the group (and the group to you)

Share your screen before speaking
Present your idea or app in 3 min. ± 1
Then you listen not converse nor interact.

You may scroll slides and use monosyllable responses; (don't use full sentences to explain or answer anything.)

Being quiet lets the group give you feedback; you are here to receive feedback, structured'ly

Feedback arrives in 3 parts (after a 15sec pause):

1. \# (aka hashtag) in a few words: the main idea heard
   
   what they'd tweet about it

1. ♥  (aka heart)
  
   what did they love about it?

   what was strong?

1. Δ  (aka delta)

   what cd b changed or improved?
   
   what confusion exists; how to clarify 

Record and plan to ask more later in the event.

Stay in the meeting to view other presentations and give feedback.

At meeting end time an emcee might ask if you have feedback about the meeting (any aspect; your choice); here you may state which of the received feedback you want more input about.

## License & Attribution

This rule-set is available under the Creative Commons CC0 1.0 License, meaning you are free to use it for any purpose, commercial or non-commercial, without any attribution back to the original authors (public domain).